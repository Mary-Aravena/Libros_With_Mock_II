package cl.marav.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class Libro {
	@Id
	@GeneratedValue
	private long id;
	private String titulo;
	private String autor;
	
	public Libro(){
		
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}
	
}
