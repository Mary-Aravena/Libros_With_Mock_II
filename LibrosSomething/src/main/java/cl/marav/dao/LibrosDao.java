package cl.marav.dao;
import org.springframework.data.repository.CrudRepository;
import cl.marav.model.Libro;

public interface LibrosDao extends CrudRepository<Libro, Long>{

		public Libro findByTitulo(String titulo);

}
