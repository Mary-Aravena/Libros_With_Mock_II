package cl.marav;
import cl.marav.dao.LibrosDao;
import cl.marav.model.Libro;
import cl.marav.service.LibrosService;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;



@RunWith(MockitoJUnitRunner.class)
public class LibrosServicesTest {

	
	@Mock
	private LibrosDao librosDao;
	
	@InjectMocks
	private LibrosService librosService;

	
	@Test
	public void deboCrearUnLibroYretornarlo() {
		//Arrange
		Libro libro= new Libro();
		
		//act
		when(librosDao.save(libro)).thenReturn(libro);
		Libro libroCreado = librosService.crearLibro(libro);

	}

}
